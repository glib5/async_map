# async_map

async-based equivalent of the built-in 'map'.

works only with functions that take exactly 1 argument.

## Example

``` python
from time import sleep

from async_map import map_async


def slow_function(n:int) -> int:
    m = n*2
    sleep(.1)
    return m


def main():
    inputs = (range(100))
    c = map_async(slow_function, inputs)
    print(c[:4])


if __name__ == "__main__":
    main()
```