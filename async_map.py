import asyncio
from typing import Any, Callable, Iterable


def map_async(func:Callable, inputs:Iterable[Any]) -> list[Any]:
    """
    async version of the built-in map.
    for functions of only 1 argument
    """
    # async version of the normal function
    async def async_func(param:Any) -> Any:
        return await asyncio.to_thread(func, param)
    # gather the group of concurrent tasks
    async def async_vec(afunc:Callable, params:Iterable[Any]) -> list[Any]:
        return await asyncio.gather(*[afunc(p) for p in params])
    # await all, exit loop, return list
    return asyncio.run(async_vec(async_func, inputs))